package com.example.andikawiyatno.reminder;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    //termporary code
    //Note mTempNote = new Note();
    private NoteAdapter mNoteAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mNoteAdapter = new NoteAdapter();
        ListView listNote = (ListView)findViewById(R.id.listView);
        listNote.setAdapter(mNoteAdapter);

        //Handle click on item
        listNote.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int whichItem, long l) {
                Note tempNote = mNoteAdapter.getItem(whichItem);
                DialogShowNote dialogShowNote = new DialogShowNote();
                dialogShowNote.sendNoteSelected(tempNote);
                dialogShowNote.show(getFragmentManager(),"");
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogNewNote dialog = new DialogNewNote();
                dialog.show(getFragmentManager(),"");
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void createNewNote(Note note){

        mNoteAdapter.addNote(note);
    }

    public class NoteAdapter extends BaseAdapter {
        List<Note> noteList = new ArrayList<Note>();
        @Override
        public int getCount(){
            return noteList.size();
        }
        @Override
        public Note getItem(int whichItem){
            return noteList.get(whichItem);
        }
        @Override
        public long getItemId(int whichItem){
            return whichItem;
        }
        @Override
        public View getView(int whichItem, View view, ViewGroup viewGroup){
            if(view == null){
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.listitem,viewGroup,false);
                TextView txtTitle = (TextView)view.findViewById(R.id.txt_title);
                TextView txtDescription = (TextView)view.findViewById(R.id.txt_description);
                ImageView imgViewIdea = (ImageView)view.findViewById(R.id.img_view_idea);
                ImageView imgViewTodo = (ImageView)view.findViewById(R.id.img_view_todo);
                ImageView imgViewImportant = (ImageView)view.findViewById(R.id.img_view_important);
                Note tempNote = noteList.get(whichItem);
                if (!tempNote.ismImportant()){
                    imgViewImportant.setVisibility(View.GONE);
                }
                if (!tempNote.ismIdea()){
                    imgViewIdea.setVisibility(View.GONE);
                }
                if(!tempNote.ismTodo()){
                    imgViewTodo.setVisibility(View.GONE);
                }
                txtTitle.setText(tempNote.getmTitle());
                txtDescription.setText(tempNote.getmDescription());
            }
            return view;
        }

        public void addNote(Note n){
            noteList.add(n);
            for (Note item : noteList)
            {
                Log.i("Info", item.getmTitle().toString());
            }
            notifyDataSetChanged();
        }
    }
}
