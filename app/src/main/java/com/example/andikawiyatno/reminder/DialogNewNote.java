package com.example.andikawiyatno.reminder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

/**
 * Created by andikawiyatno on 1/9/18.
 */

public class DialogNewNote extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_note,null);
        final EditText editTitle = (EditText)dialogView.findViewById(R.id.edt_title);
        final EditText editDescription = (EditText)dialogView.findViewById(R.id.edt_description);
        final CheckBox checkBoxIdea = (CheckBox)dialogView.findViewById(R.id.cb_idea);
        final CheckBox checkBoxTodo = (CheckBox)dialogView.findViewById(R.id.cb_todo);
        final CheckBox checkBoxImportant = (CheckBox)dialogView.findViewById(R.id.cb_important);
        final Button btnCancel = (Button)dialogView.findViewById(R.id.btn_cancel);
        final Button btnOk = (Button)dialogView.findViewById(R.id.btn_ok);
        builder.setView(dialogView).setMessage("Add new note");

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Note newNote = new Note();
                newNote.setmTitle(editTitle.getText().toString());
                newNote.setmDescription(editDescription.getText().toString());
                newNote.setmIdea(checkBoxIdea.isChecked());
                newNote.setmTodo(checkBoxTodo.isChecked());
                newNote.setmImportant(checkBoxImportant.isChecked());

                if (checkBoxIdea.isChecked()){
                    Log.i("Info", "Idea di checklist:"+newNote.getmTitle());
                }
                if(checkBoxTodo.isChecked()){
                    Log.i("Info", "Todo di checklist:"+newNote.getmTitle());
                }
                if(checkBoxImportant.isChecked()){
                    Log.i("Info", "Important di checklist:"+newNote.getmTitle());
                }

                MainActivity callingActivity = (MainActivity)getActivity();
                callingActivity.createNewNote(newNote);
                dismiss();
            }
        });

        return builder.create();
    }
}
