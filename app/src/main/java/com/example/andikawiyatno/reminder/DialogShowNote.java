package com.example.andikawiyatno.reminder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by andikawiyatno on 1/9/18.
 */

public class DialogShowNote extends DialogFragment {
    private Note mNote;

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.dialog_show_note,null);
        TextView txtTitle = (TextView)dialogView.findViewById(R.id.txt_title);
        TextView txtDescription = (TextView)dialogView.findViewById(R.id.txt_description);
        Button btnOk = (Button)dialogView.findViewById(R.id.btn_ok);
        txtTitle.setText(mNote.getmTitle());
        txtDescription.setText(mNote.getmDescription());
        ImageView imgImportant = (ImageView)dialogView.findViewById(R.id.img_view_idea);
        ImageView imgIdea = (ImageView)dialogView.findViewById(R.id.img_view_idea);
        ImageView imgTodo = (ImageView)dialogView.findViewById(R.id.img_view_todo);

        if (!mNote.ismImportant()){
            imgImportant.setVisibility(View.GONE);
        }
        if (!mNote.ismTodo()){
            imgTodo.setVisibility(View.GONE);
        }
        if(!mNote.ismIdea()){
            imgIdea.setVisibility(View.GONE);
        }

        builder.setView(dialogView).setMessage("Your Note");
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return builder.create();
    }

    public void sendNoteSelected(Note noteSelected){
        mNote = noteSelected;
    }

}
